package library;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import library.data.Author;
import library.data.Book;
import library.data.BookGenre;
import library.data.PubHouse;
import library.filter.StdBookFilter;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class HibernateLibraryTest {

	static private Library library;
	static private List<Object> backup = new ArrayList<Object>();

	@BeforeClass
	static public void setUpBeforeClass() throws Exception {
		library = HibernateLibrary.getInstance();
		backup.addAll(library.getAuthorList());
		backup.addAll(library.getGenreList());
		backup.addAll(library.getPubHouseList());
		backup.addAll(library.getBookList());
	}
	@Before
	public void setUpBefore() throws Exception {
		library.deleteAll();
	}

	@Test
	public void testDeleteAll() {
		library.saveOrUpdate(new Book("test", new Author("test", null),
				new Integer(0), new PubHouse("pubhouse"), new BookGenre("genre")));
		assertNotEquals(library.getBookList().size(), 0);
		assertNotEquals(library.getGenreList().size(), 0);
		assertNotEquals(library.getAuthorList().size(), 0);
		assertNotEquals(library.getPubHouseList().size(), 0);
		library.deleteAll();
		assertEquals(library.getBookList().size(), 0);
		assertEquals(library.getGenreList().size(), 0);
		assertEquals(library.getAuthorList().size(), 0);
		assertEquals(library.getPubHouseList().size(), 0);
				
	}
	
	@Test
	public void testDeleteBook() {
		Book testBook1 = new Book("test1", null, 0, null, null);
		library.saveOrUpdate(testBook1);

		Book testBook2 = new Book("test2", null, 0, null, null);
		library.saveOrUpdate(testBook2);

		assertTrue(library.getBookList().contains(testBook1));
		assertTrue(library.getBookList().contains(testBook2));
		
		library.delete(testBook2);
		assertTrue(library.getBookList().contains(testBook1));
		assertFalse(library.getBookList().contains(testBook2));
	}

	@Test
	public void testDeleteCollectionOfBook() {
		// create test lists
		List<Book> testList1 = new ArrayList<Book>();
		for(int i = 0; i < 10; i++) {
			testList1.add(new Book("tytuł" + i, null, i, null, null));
		}
		List<Book> testList2 = new ArrayList<Book>();
		for(int i = 0; i < 10; i++)
			testList2.add(new Book("tytuł" + (i+20), null, i+20, null, null));
		
		// Save test lists to database
		for(Object obj: testList1)
			library.saveOrUpdate(obj);
		for(Object obj: testList2)
			library.saveOrUpdate(obj);
		
		// check if added correctly
		for(Object obj: testList1)
			assertTrue(library.getBookList().contains(obj));
		for(Object obj: testList2)
			assertTrue(library.getBookList().contains(obj));	
		
		// delete one of test lists
		for(Book obj: testList2)
			library.delete(obj);
		
		// check if deleted correctly
		for(Object obj: testList1)
			assertTrue(library.getBookList().contains(obj));		
		for(Object obj: testList2)
			assertFalse(library.getBookList().contains(obj));		
	}

	@Test
	public void testSaveOrUpdate() {
		Book testBook1 = new Book("test1", null, 0, null, null);
		Book testBook2 = new Book("test2", null, 0, null, null);
		assertFalse(library.getBookList().contains(testBook1));
		assertFalse(library.getBookList().contains(testBook2));
		
		library.saveOrUpdate(testBook1);

		assertTrue(library.getBookList().contains(testBook1));
		assertFalse(library.getBookList().contains(testBook2));
		
	}

	@Test
	public void testSetBookFilter() {
		Random rand = new Random();
		//create test lists
		Author testAuthor = new Author("yesincludeMe",null);
		PubHouse testHouse = new PubHouse("yesincludedHouse");
		BookGenre testgenre = new BookGenre("yesincludegenre");
		List<Book> testList1 = new ArrayList<Book>();
		for (int i = 0; i < 10; i++) {
			testList1.add(new Book(rand.nextInt() + "include" + rand.nextInt(),
					testAuthor, rand.nextInt(78) + 21, testHouse, testgenre));
		}
		testAuthor = new Author("noexcludeMe",null);
		testHouse = new PubHouse("noexcludedHouse");
		testgenre = new BookGenre("noexcludegenre");
		List<Book> testList2 = new ArrayList<Book>();
		for (int i = 0; i < 5; i++) {
			testList2.add(new Book(rand.nextInt() + "exclude" + rand.nextInt(),
					testAuthor, rand.nextInt(20), testHouse, testgenre));
			testList2.add(new Book(rand.nextInt() + "exclude" + rand.nextInt(),
					testAuthor, rand.nextInt(20) + 100, testHouse, testgenre));
		}
		
		// add books to library
		for(Object obj: testList1)
			library.saveOrUpdate(obj);
		for(Object obj: testList2)
			library.saveOrUpdate(obj);

		// check if added correctly
		for(Object obj: testList1)
			assertTrue(library.getBookList().contains(obj));
		for(Object obj: testList2)
			assertTrue(library.getBookList().contains(obj));	

		// set filter
		library.setBookFilter(new StdBookFilter("include", null, null, null, null, null));
		// test if filtered
		for (Object obj : testList1)
			assertTrue(library.getBookList().contains(obj));
		for (Object obj : testList2)
			assertFalse(library.getBookList().contains(obj));
		
		// set filter
		library.setBookFilter(new StdBookFilter(null, "exclude", null, null, null, null));
		// test if filtered
		for (Object obj : testList1)
			assertFalse(library.getBookList().contains(obj));
		for (Object obj : testList2)
			assertTrue(library.getBookList().contains(obj));

		// set filter
		library.setBookFilter(new StdBookFilter(null, null, "exclude", null, null, null));
		// test if filtered
		for (Object obj : testList1)
			assertFalse(library.getBookList().contains(obj));
		for (Object obj : testList2)
			assertTrue(library.getBookList().contains(obj));

		// set filter
		library.setBookFilter(new StdBookFilter(null, null, "include", null, null, null));
		// test if filtered
		for (Object obj : testList1)
			assertTrue(library.getBookList().contains(obj));
		for (Object obj : testList2)
			assertFalse(library.getBookList().contains(obj));

		// set filter
		library.setBookFilter(new StdBookFilter(null, null, null, "include", null, null));
		// test if filtered
		for (Object obj : testList1)
			assertTrue(library.getBookList().contains(obj));
		for (Object obj : testList2)
			assertFalse(library.getBookList().contains(obj));

		// set filter
		library.setBookFilter(new StdBookFilter(null, null, null, null, 21, 99));
		// test if filtered
		for (Object obj : testList1)
			assertTrue(library.getBookList().contains(obj));
		for (Object obj : testList2)
			assertFalse(library.getBookList().contains(obj));
		
		// clear filter
		library.clearFilter();
		// test if filtered
		for (Object obj : testList1)
			assertTrue(library.getBookList().contains(obj));
		for (Object obj : testList2)
			assertTrue(library.getBookList().contains(obj));
	}

	@Test
	public void testGetBookListHeaders() {
		assertArrayEquals(library.getBookListHeader(), Book.getHeader());
	}

	@AfterClass
	static public void afterClass() throws Exception {
		library.deleteAll();
		for (Object obj: backup) {
			library.saveOrUpdate(obj);
		}
	}
}
