package library.data;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class PubHouseTest {

	@Test
	public void testPubHouseString() {
		PubHouse pubHouse= new PubHouse("name");
		assertEquals(pubHouse.getName(), "name");
		
	}

	@Test
	public void testSetName() {
		PubHouse pubHouse= new PubHouse();
		pubHouse.setName("name");
		assertEquals(pubHouse.getName(), "name");
	}

}
