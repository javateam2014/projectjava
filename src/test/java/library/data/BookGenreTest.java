package library.data;

import static org.junit.Assert.*;

import org.junit.Test;

public class BookGenreTest {

	@Test
	public void testBookGenreString() {
		BookGenre genre = new BookGenre("name");
		assertEquals(genre.getName(), "name");
	}

	@Test
	public void testSetName() {
		BookGenre genre = new BookGenre();
		genre.setName("name");
		assertEquals(genre.getName(), "name");
	}

}
