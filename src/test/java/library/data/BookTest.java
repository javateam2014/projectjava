package library.data;

import static org.junit.Assert.*;

import org.junit.Test;

public class BookTest {

	@Test
	public void testSetTitle() {
		Book book = new Book();
		book.setTitle("tytuł");
		assertEquals(book.getTitle(), "tytuł");
	}
	
	@Test
	public void testSetGenre() {
		Book book = new Book();
		BookGenre bookGenre = new BookGenre();
		book.setGenre(bookGenre);
		assertEquals(book.getGenre(), bookGenre);
	}
	@Test
	public void testSetAuthor() {
		Book book = new Book();
		Author author = new Author();
		book.setAuthor(author);
		assertEquals(book.getAuthor(),author);
	}
	@Test
	public void testSetPubHouse() {
		Book book = new Book();
		PubHouse pubHouse = new PubHouse();
		book.setPubHouse(pubHouse);
		assertEquals(book.getPubHouse(),pubHouse);
	}
	@Test
	public void testSetPubYear() {
		Book book = new Book();
		book.setPubYear(2000);
		assertEquals(book.getPubYear(), new Integer(2000));
	}
	

}
