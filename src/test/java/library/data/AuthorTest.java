package library.data;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;

@SuppressWarnings("deprecation")
public class AuthorTest {

	@Test
	public void testAuthorStringDate() {
		Author author = new Author("name", new Date(1,2,3));
		assertEquals(author.getBirthDate(), new Date(1,2,3));
		assertEquals(author.getName(), "name");
	}

	@Test
	public void testSetName() {
		Author author = new Author();
		author.setName("name");
		assertEquals(author.getName(), "name");
	}

	@Test
	public void testSetBirthDate() {
		Author author = new Author();
		author.setBirthDate(new Date(1,2,3));
		assertEquals(author.getBirthDate(), new Date(1,2,3));
	}

}
