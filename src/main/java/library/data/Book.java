package library.data;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import org.hibernate.Session;

@Entity
public class Book {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	protected Long id;

	@Column(name = "TITLE", unique = false, nullable = false)
	protected String title;

	@ManyToOne
    @JoinColumn(name="AUTHOR")
	protected Author author;

	@Column(name = "PUB_YEAR")
	protected Integer pubYear;

	@ManyToOne
    @JoinColumn(name = "PUB_HOUSE_ID")
	protected PubHouse pubHouse;
	
	@ManyToOne
    @JoinColumn(name = "GENRE_ID")
	protected BookGenre genre;
	
	public Book() {}
	
	public Book(String title, Author author, Integer pubYear,
			PubHouse pubHouse, BookGenre genre) {
		super();
		this.title = title;
		this.author = author;
		this.pubYear = pubYear;
		this.pubHouse = pubHouse;
		this.genre = genre;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public PubHouse getPubHouse() {
		return pubHouse;
	}

	public void setPubHouse(PubHouse pubHouse) {
		this.pubHouse = pubHouse;
	}

	public BookGenre getGenre() {
		return genre;
	}

	public void setGenre(BookGenre genre) {
		this.genre = genre;
	}

	public Integer getPubYear() {
		return pubYear;
	}

	public void setPubYear(int pubYear) {
		this.pubYear = pubYear;
	}

	public Object[] explode() {
		ArrayList<Object> result = new ArrayList<Object>();

		result.add(this.getTitle());
		result.add(this.getAuthor());
		result.add(this.getGenre());
		result.add(this.getPubYear());
		result.add(this.getPubHouse());
		
		result.add(this);
		return result.toArray();
	}
	
	public static String[] getHeader() {
		ArrayList<String> result = new ArrayList<String>();

		result.add("Title");
		result.add("Author");
		result.add("Genre");
		result.add("Publish Year");
		result.add("Publishing House");
		
		return result.toArray(new String[0]);
	}
	
	public void saveDeps(Session session) {
		if(this.author != null)
			session.saveOrUpdate(this.author);
		if(this.genre != null)
			session.saveOrUpdate(this.genre);
		if(this.pubHouse != null)
			session.saveOrUpdate(this.pubHouse);
	}

	public void updateDeps(Session session) {
		session.update(this.author);
		session.update(this.genre);
		session.update(this.pubHouse);
		
	}

}
