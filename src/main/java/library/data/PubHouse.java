package library.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class PubHouse {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "PUB_HOUSE_ID", unique = true, nullable = false)
	@OneToMany(mappedBy = "BOOK")
	private Long id;
	
	@Column (name = "NAME", unique = false, nullable = false)
	private String name;

	public String toString() {
		return name;
	}

	public PubHouse(String name) {
		super();
		this.name = name;
	}
	public PubHouse() {}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
