package library.filter;

import java.util.ArrayList;
import java.util.List;

import library.data.Book;

public class RegexBookFilter extends BookFilter  {
	private String title;
	private String author;
	private String genre;
	private String pubHouse;
	private Integer pubYearDown;
	private Integer pubYearUp;
	
	public RegexBookFilter(String title, String author, String genre,
			String pubHouse, Integer pubYearDown, Integer pubYearUp) {
		this.title = title;
		this.author = author;
		this.genre = genre;
		this.pubHouse = pubHouse;
		this.pubYearDown = pubYearDown;
		this.pubYearUp = pubYearUp;
	}


	protected List<Book> inclusiveFilter(List<Book> bookList) { 
		List<Book> result = new ArrayList<Book>();
		for (Book book : bookList) {
			if (!((this.title != null
						&& !book.getTitle().matches(this.title))
					|| (this.author != null
						&& !book.getAuthor().toString().matches(this.author))
					|| (this.genre != null
						&& !book.getGenre().toString().matches(this.genre))
					|| (this.pubHouse != null
						&& !book.getPubHouse().toString().matches(pubHouse))
					|| (this.pubYearDown != null
						&& (this.pubYearDown > book.getPubYear()))
					|| (this.pubYearUp != null
						&& (this.pubYearUp < book.getPubYear())))) {
				result.add(book);
			}
		}
		return result;
	}

	protected List<Book> exclusiveFilter(List<Book> bookList) {
		List<Book> result = new ArrayList<Book>();
		for (Book book : bookList) {
			if (((this.title != null
						&& !book.getTitle().matches(this.title))
					|| (this.author != null
						&& !book.getAuthor().toString().matches(this.author))
					|| (this.genre != null
						&& !book.getGenre().toString().matches(this.genre))
					|| (this.pubHouse != null
						&& !book.getPubHouse().toString().matches(this.pubHouse))
					|| (this.pubYearDown != null
						&& (this.pubYearDown > book.getPubYear()))
					|| (this.pubYearUp != null
						&& (this.pubYearUp < book.getPubYear())))) {
				result.add(book);
			}
		}
		return result;
	}


}
