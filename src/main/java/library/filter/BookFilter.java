package library.filter;

import java.util.List;

import library.data.Book;

public abstract class BookFilter {
	protected boolean exclude = false;
	
	protected abstract List<Book> inclusiveFilter(List<Book> bookList);
	protected abstract List<Book> exclusiveFilter(List<Book> bookList);
	
	public List<Book> filter(List<Book> bookList) {
		if(exclude)
			return exclusiveFilter(bookList);
		else
			return inclusiveFilter(bookList);
	}


	public void exclude(boolean b) {
		this.exclude = b;
	}
}
