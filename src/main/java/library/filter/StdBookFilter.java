package library.filter;

import java.util.ArrayList;
import java.util.List;

import library.data.Book;

public class StdBookFilter extends BookFilter {
	private String title;
	private String author;
	private String genre;
	private String pubHouse;
	private Integer pubYearDown;
	private Integer pubYearUp;

	public StdBookFilter(String title, String author, String genre,
			String pubHouse, Integer pubYearDown, Integer pubYearUp) {
		this.title = title;
		this.author = author;
		this.genre = genre;
		this.pubHouse = pubHouse;
		this.pubYearDown = pubYearDown;
		this.pubYearUp = pubYearUp;
	}
	
	protected List<Book> inclusiveFilter(List<Book> bookList) {
		List<Book> result = new ArrayList<Book>();
		for (Book book : bookList) {
			if (!((this.title != null && !book.getTitle().contains(this.title))					
					|| (this.author != null && !book.getAuthor().toString()
							.contains(this.author))
					|| (this.genre != null && !book.getGenre().toString()
							.contains(this.genre))
					|| (this.pubHouse != null && !book.getPubHouse().toString()
							.contains(this.pubHouse))
					|| (this.pubYearDown != null && (this.pubYearDown > book
							.getPubYear()))
					|| (this.pubYearUp != null && (this.pubYearUp < book
							.getPubYear())))) {
				result.add(book);
			}
		}
		return result;
	}

	protected List<Book> exclusiveFilter(List<Book> bookList) {
		List<Book> result = new ArrayList<Book>();
		for (Book book : bookList) {
			if ((this.title != null && !book.getTitle().contains(this.title))					
					|| (this.author != null && !book.getAuthor().toString()
							.contains(this.author))
					|| (this.genre != null && !book.getGenre().toString()
							.contains(this.genre))
					|| (this.pubHouse != null && !book.getPubHouse().toString()
							.contains(this.pubHouse))
					|| (this.pubYearDown != null && (this.pubYearDown > book
							.getPubYear()))
					|| (this.pubYearUp != null && (this.pubYearUp < book
							.getPubYear()))) {
				result.add(book);
			}
		}
		return result;
	}
}
