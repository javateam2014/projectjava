package library;

import java.util.Collection;
import java.util.List;

import library.data.Author;
import library.data.Book;
import library.data.BookGenre;
import library.data.PubHouse;
import library.filter.BookFilter;

public interface Library {
	public void delete(Object object);

	@SuppressWarnings("rawtypes")
	void delete(Collection col);

	void saveOrUpdate(Object obj);

	/**
	 * Get array of books split into array of class fields and this object at
	 * the end
	 */
	Object[][] getExplodedBooks();

	/**
	 * List of headers (intended to use with getExplodedBook())
	 * 
	 * @return Array of headers
	 */
	String[] getBookListHeader();

	/**
	 * @return List of authors
	 */
	List<Author> getAuthorList();

	/**
	 * @return List of genres
	 */
	List<BookGenre> getGenreList();

	/**
	 * @return List of publishing houses
	 */
	List<PubHouse> getPubHouseList();

	/**
	 * Wipes library
	 */
	void deleteAll ();
	
	void setBookFilter(BookFilter filter);

	void clearFilter();

	List<Book> getBookList();

	public void close();
	
}
