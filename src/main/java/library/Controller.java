package library;

import library.gui.Window;

public class Controller { // Controller
	private Library library; // model in MVC
	private Window window; // model in MVC

	/**
	 * Constructor for controller
	 */
	public Controller() {
		this.library = HibernateLibrary.getInstance();
		this.window= new Window(library);
		window.setVisible(true);
	}

	/**
	 * Start point of application
	 */
	public static void main(String[] args) {
		new Controller();
	}

}
