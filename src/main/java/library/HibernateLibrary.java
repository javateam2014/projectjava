package library;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import library.data.Author;
import library.data.Book;
import library.data.BookGenre;
import library.data.PubHouse;
import library.filter.BookFilter;

@SuppressWarnings("unchecked")
public class HibernateLibrary implements Library { 
	private List<Book> bookList;
	private List<Author> authorList;
	private List<BookGenre> genreList;
	private List<PubHouse> pubHouseList;
	private BookFilter filter = null;
	private Session session;
	
	static private HibernateLibrary instance;
	
	static HibernateLibrary getInstance() {
		if(instance == null)
			instance = new HibernateLibrary(); // Lazy initialization pattern
		return instance;
	}
	
	private HibernateLibrary() {
		session = HibernateUtil.getSessionFactory().openSession();
		readData();
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				HibernateLibrary.this.close();
			}
		});
	}

	private void readData() {
		bookList = session.createQuery("from Book").list();
		authorList = session.createQuery("from Author").list();
		genreList = session.createQuery("from BookGenre").list();
		pubHouseList = session.createQuery("from PubHouse").list();
	}

	@Override
	public void delete(Object obj) {
		Transaction tx = session.beginTransaction();
		
		if (obj instanceof Book){
			if (!bookList.contains(obj)) {
				bookList.remove(obj);
			}
			session.delete(obj);
		} else if (obj instanceof Author) {
			if (!authorList.contains(obj)) {
				authorList.remove(obj);
			}
			session.delete(obj);
		} else if (obj instanceof PubHouse) {
			if (!pubHouseList.contains(obj)) {
				pubHouseList.remove(obj);
			}
			session.delete(obj);
		} else if (obj instanceof BookGenre) {
			if (!genreList.contains(obj)) {
				genreList.remove(obj);
			}
			session.delete(obj);
		} else
			throw new RuntimeException("LibrarySingleton: Unknown type to save");
		
		tx.commit();
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void delete(Collection col) {
		for(Object obj: col){
			this.delete(obj);
		}
	}

	@Override
	public void saveOrUpdate(Object obj) {
		Transaction tx = session.beginTransaction();
				
		if (obj instanceof Book){
			if (!bookList.contains(obj)) {
				bookList.add((Book) obj);
			}
			((Book) obj).saveDeps(session);
			session.saveOrUpdate(obj);
		} else if (obj instanceof Author) {
			if (!authorList.contains(obj)) {
				authorList.add((Author) obj);
			}
			session.saveOrUpdate(obj);
		} else if (obj instanceof PubHouse) {
			if (!pubHouseList.contains(obj)) {
				pubHouseList.add((PubHouse) obj);
			}
			session.saveOrUpdate(obj);
		} else if (obj instanceof BookGenre) {
			if (!genreList.contains(obj)) {
				genreList.add((BookGenre) obj);
			}
			session.saveOrUpdate(obj);
		} else
			throw new RuntimeException("LibrarySingleton: Unknown type to save");

		tx.commit();
	}

	@Override
	public void setBookFilter(BookFilter filter) {
		this.filter = filter;
	}

	@Override
	public void clearFilter() {
		this.filter = null;
	}

	/**
	 * Return array of books split (into inner arrays) by class fields
	 * book is last object in inner arrays
	 */
	@Override
 	public Object[][] getExplodedBooks() {
		ArrayList<Object[]> result = new ArrayList<Object[]>();
		
		for (Book tmp: this.getBookList() ) {
			result.add(tmp.explode());
		}
		
		return result.toArray(new Object[0][0]);
	}
	
	/**
	 * List of headers (intended to use with getExplodedList())
	 * @return Array of headers
	 */
	@Override
	public String[] getBookListHeader() {
		return Book.getHeader();
	}

	@Override
	public List<Book> getBookList() {
		bookList = session.createQuery("from Book").list();
		if(this.filter == null)
			return bookList;
		return this.filter.filter(bookList);
	}

	/**
	 * @return List of authors
	 */
	@Override
	public List<Author> getAuthorList() {
		authorList = session.createQuery("from Author").list();
		return authorList;
	}

	/**
	 * @return List of genres
	 */
	@Override
	public List<BookGenre> getGenreList() {
		genreList = session.createQuery("from BookGenre").list();
		return genreList;
	}

	/**
	 * @return List of publishing houses
	 */
	@Override
	public List<PubHouse> getPubHouseList() {
		pubHouseList = session.createQuery("from PubHouse").list();
		return pubHouseList;
	}

	@Override
	public void deleteAll() {
		Transaction tx = session.beginTransaction();
		session.createQuery("DELETE FROM Book").executeUpdate();
		session.createQuery("DELETE FROM Author").executeUpdate();
		session.createQuery("DELETE FROM BookGenre").executeUpdate();
		session.createQuery("DELETE FROM PubHouse").executeUpdate();
		tx.commit();
		session.clear();
		readData();
	}

	@Override
	public void close() {
		Transaction tx = session.beginTransaction();
		for (Book book: bookList) {
			book.saveDeps(session);
			session.saveOrUpdate(book);
		}
		tx.commit();
		session.close();
	}

}
