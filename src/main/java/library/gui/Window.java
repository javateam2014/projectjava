package library.gui;

import javax.swing.JFrame;

import java.awt.BorderLayout;

import javax.swing.ButtonGroup;
import javax.swing.JDialog;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JSplitPane;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Dimension;

import javax.swing.JTable;

import java.awt.event.MouseAdapter;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JRadioButton;

import library.Library;
import library.data.Book;
import library.filter.BookFilter;
import library.filter.RegexBookFilter;
import library.filter.StdBookFilter;

import javax.swing.JPopupMenu;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.JCheckBox;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.DefaultTableModel;

public class Window {
	private JFrame frame;
	private JTextField txtTitle;
	private JTextField txtAuthor;
	private JTextField txtStartYear;
	private JTextField txtStopYear;
	private JTextField txtPubHouse;
	private JSplitPane splitPane;
	private JButton btnAddBook;
	private JPopupMenu menu;
	private JRadioButton rdbtnNone;
	private JRadioButton rdbtnStandard;
	private JRadioButton rdbtnRegex;

	private Library library;
	private JTable table;
	private JTextField txtGenre;
	private String filterTitle;
	private String filterAuthor;
	private String filterGenre;
	private String filterPubHouse;
	private Integer filterPubYearStart;
	private Integer filterPubYearStop;
	private JCheckBox chckbxExclude;
	private JButton btnEditPublishingHouses;
	private JButton btnEditAuthors;
	private JButton btnEditGenres;
	
	/**
	 * Create the application.
	 */
	public Window(Library library) {
		this.library = library;
		initialize();
		initializeListeners();
	}

	/**
	 * Initialize listeners.
	 */
	private void initializeListeners() {
		txtTitle.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) { update();}
			public void removeUpdate(DocumentEvent e) { update();}
			public void insertUpdate(DocumentEvent e) { update();}
			private void update() {
				if (!txtTitle.getText().isEmpty()) {
					filterTitle = txtTitle.getText();
				} else {
					filterTitle = null;
				}
				refreshFilter();
				
			}

		});

		txtAuthor.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) { update();}
			public void removeUpdate(DocumentEvent e) { update();}
			public void insertUpdate(DocumentEvent e) { update();}
			private void update() {
				if (!txtAuthor.getText().isEmpty()) {
					filterAuthor = txtAuthor.getText();
				} else {
					filterAuthor = null;
				}
				refreshFilter();
			}
		});
		
		txtStartYear.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) { update();}
			public void removeUpdate(DocumentEvent e) { update();}
			public void insertUpdate(DocumentEvent e) { update();}
			private void update() {
				if (!txtStartYear.getText().isEmpty()) {
					try {
						filterPubYearStart = Integer.parseInt(
								txtStartYear.getText());
						txtStartYear.setBackground(new Color(0xFFFFFF));
					} catch (NumberFormatException ex) {
						txtStartYear.setBackground(new Color(0xAA0000));
						filterPubYearStart = null;
					}
				} else {
					txtStartYear.setBackground(new Color(0xFFFFFF));
					filterPubYearStart = null;
				}
				refreshFilter();
			}

		});

		txtStopYear.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) { update();}
			public void removeUpdate(DocumentEvent e) { update();}
			public void insertUpdate(DocumentEvent e) { update();}
			private void update() {
				if (!txtStopYear.getText().isEmpty()) {
					try {
						filterPubYearStop = Integer.parseInt(
								txtStopYear.getText());
						txtStopYear.setBackground(new Color(0xFFFFFF));
					} catch (NumberFormatException ex) {
						txtStopYear.setBackground(new Color(0xAA0000));
						filterPubYearStop = null;
					}
				} else {
					txtStopYear.setBackground(new Color(0xFFFFFF));
					filterPubYearStop = null;
				}
				refreshFilter();
			}
		});
		
		txtPubHouse.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) { update();}
			public void removeUpdate(DocumentEvent e) { update();}
			public void insertUpdate(DocumentEvent e) { update();}
			private void update() {
				if (!txtPubHouse.getText().isEmpty()) {
					filterPubHouse = txtPubHouse.getText();
				} else {
					filterPubHouse = null;
				}
				refreshFilter();
			}
		});
		
		txtGenre.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) { update();}
			public void removeUpdate(DocumentEvent e) { update();}
			public void insertUpdate(DocumentEvent e) { update();}
			private void update() {
				if (!txtGenre.getText().isEmpty()) {
					filterGenre = txtGenre.getText();
				} else {
					filterGenre = null;
				}
				refreshFilter();
			}
		});

		btnAddBook.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JDialog dialog = new BookDialog(library);
				dialog.setVisible(true);
				reloadMainTable();
			}
		});
		
		chckbxExclude.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				refreshFilter();
			}
		});

		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final java.awt.event.MouseEvent event) {
				final int row = table.rowAtPoint(event.getPoint());
				if (event.getButton() == 3 && row != -1) {	
					menu.removeAll();
					{
						if (table.getSelectedRowCount() == 1) {
							JMenuItem edit = new JMenuItem("edit");
							edit.addActionListener(new ActionListener() {
								@Override
								public void actionPerformed(ActionEvent e) {
									JDialog dialog = new BookDialog(library,
											(Book) table.getModel().getValueAt(
													table.getSelectedRow(),
													Book.getHeader().length));
									dialog.setVisible(true);
									reloadMainTable();
									
								}
							});
							menu.add(edit);
							
							JMenuItem delete = new JMenuItem("delete");
							delete.addActionListener(new ActionListener() {
								@Override
								public void actionPerformed(ActionEvent e) {
									library.delete((Book) table.getModel().getValueAt(
											table.getSelectedRow(),
											Book.getHeader().length));
									reloadMainTable();
									
								}
							});
							menu.add(delete);
							
						}  else if (table.getSelectedRowCount() > 1) {
							final List<Book> bookList = new ArrayList<Book>();

							for (int i : table.getSelectedRows()) {
								bookList.add((Book) table.getModel().getValueAt(
											i, Book.getHeader().length));
											
							}
							JMenuItem delete = new JMenuItem("delete");
							delete.addActionListener(new ActionListener() {
								@Override
								public void actionPerformed(ActionEvent e) {
									library.delete(bookList);
									reloadMainTable();
									
								}
							});
							menu.add(delete);
							
						}
					}
					menu.show(table,event.getX(),event.getY());
				}
			}
		});

		btnEditAuthors.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JDialog dialog = new EditAuthorsDialog(library);
				dialog.setVisible(true);
				reloadMainTable();
			}
		});
		
		btnEditGenres.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JDialog dialog = new EditGenresDialog(library);
				dialog.setVisible(true);
				reloadMainTable();
			}
		});
		
		btnEditPublishingHouses.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JDialog dialog = new EditPubHousesDialog(library);
				dialog.setVisible(true);
				reloadMainTable();
			}
		});
	
	}
	
	/**
	 * reload filter
	 */
	protected void refreshFilter() {
		
		if (rdbtnNone.isSelected()) {
			library.clearFilter();
		} else {
			BookFilter filter;
			if (rdbtnStandard.isSelected()) {
				filter = new StdBookFilter(filterTitle, filterAuthor, filterGenre,
						filterPubHouse, filterPubYearStart, filterPubYearStop);

			} else {
				filter = new RegexBookFilter(filterTitle, filterAuthor, filterGenre,
						filterPubHouse, filterPubYearStart, filterPubYearStop);

			}
			if (chckbxExclude.isSelected())
				filter.exclude(true);
			library.setBookFilter(filter);
			
		}
		reloadMainTable();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setSize(new Dimension(769, 461));
		frame.setPreferredSize(new Dimension(800, 500));
		frame.setMinimumSize(new Dimension(500, 300));
		frame.setBounds(100, 100, 548, 457);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		splitPane = new JSplitPane();
		frame.getContentPane().add(splitPane, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		panel.setPreferredSize(new Dimension(300, 10));
		splitPane.setLeftComponent(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[] {100, 0};
		gbl_panel.rowHeights = new int[]{0, 26, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{1.0, 0.0};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JPanel panel_3 = new JPanel();
		panel_3.setAlignmentX(Component.LEFT_ALIGNMENT);
		panel_3.setPreferredSize(new Dimension(300, 32));
		panel_3.setMinimumSize(new Dimension(300, 0));
		panel_3.getLayout();
		
		JLabel lblTitle = new JLabel("Title:");
		panel_3.add(lblTitle);
		
		txtTitle = new JTextField();
		panel_3.add(txtTitle);
		
		JPanel panel_2 = new JPanel();
		GridBagConstraints gbc_panel_2 = new GridBagConstraints();
		gbc_panel_2.gridwidth = 2;
		gbc_panel_2.insets = new Insets(0, 0, 5, 5);
		gbc_panel_2.fill = GridBagConstraints.BOTH;
		gbc_panel_2.gridx = 0;
		gbc_panel_2.gridy = 0;
		panel.add(panel_2, gbc_panel_2);
		
		JLabel lblFilter = new JLabel("Filter:");
		panel_2.add(lblFilter);
		
		chckbxExclude = new JCheckBox("Exclude");
		panel_2.add(chckbxExclude);
		
		JPanel panel_8 = new JPanel();
		panel_8.setPreferredSize(new Dimension(300, 32));
		panel_8.setMinimumSize(new Dimension(300, 0));
		panel_8.setAlignmentX(0.0f);
		GridBagConstraints gbc_panel_8 = new GridBagConstraints();
		gbc_panel_8.gridwidth = 2;
		gbc_panel_8.anchor = GridBagConstraints.NORTH;
		gbc_panel_8.insets = new Insets(0, 0, 5, 5);
		gbc_panel_8.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel_8.gridx = 0;
		gbc_panel_8.gridy = 1;
		panel.add(panel_8, gbc_panel_8);
		
		ButtonGroup buttonGroup = new ButtonGroup();
		rdbtnNone = new JRadioButton("None");
		rdbtnNone.setSelected(true);
		buttonGroup.add(rdbtnNone);
		panel_8.add(rdbtnNone);
		
		rdbtnStandard = new JRadioButton("Standard");
		buttonGroup.add(rdbtnStandard);
		panel_8.add(rdbtnStandard);
		
		rdbtnRegex = new JRadioButton("Regex");
		buttonGroup.add(rdbtnRegex);
		panel_8.add(rdbtnRegex);
		
		txtTitle.setHorizontalAlignment(SwingConstants.CENTER);
		txtTitle.setColumns(10);
		GridBagConstraints gbc_panel_3 = new GridBagConstraints();
		gbc_panel_3.anchor = GridBagConstraints.NORTHWEST;
		gbc_panel_3.insets = new Insets(0, 0, 5, 5);
		gbc_panel_3.gridx = 0;
		gbc_panel_3.gridy = 2;
		panel.add(panel_3, gbc_panel_3);
		
		JPanel panel_4 = new JPanel();
		panel_4.setAlignmentX(Component.LEFT_ALIGNMENT);
		panel_4.setPreferredSize(new Dimension(300, 32));
		panel_4.setMinimumSize(new Dimension(300, 0));
		panel_4.getLayout();
		
		JLabel lblAuthor = new JLabel("Author:");
		panel_4.add(lblAuthor);
		
		txtAuthor = new JTextField();
		panel_4.add(txtAuthor);
		txtAuthor.setHorizontalAlignment(SwingConstants.CENTER);
		txtAuthor.setColumns(10);
		GridBagConstraints gbc_panel_4 = new GridBagConstraints();
		gbc_panel_4.gridwidth = 2;
		gbc_panel_4.anchor = GridBagConstraints.NORTHWEST;
		gbc_panel_4.insets = new Insets(0, 0, 5, 5);
		gbc_panel_4.gridx = 0;
		gbc_panel_4.gridy = 3;
		panel.add(panel_4, gbc_panel_4);
		
		JPanel panel_5 = new JPanel();
		panel_5.setAlignmentX(Component.LEFT_ALIGNMENT);
		panel_5.setPreferredSize(new Dimension(300, 32));
		panel_5.setMinimumSize(new Dimension(300, 0));
		panel_5.getLayout();
		
		
		JLabel lblPubYear = new JLabel("Pub Year:");
		panel_5.add(lblPubYear);
		
		txtStartYear = new JTextField();
		panel_5.add(txtStartYear);
		txtStartYear.setHorizontalAlignment(SwingConstants.CENTER);
		txtStartYear.setColumns(4);
		
		
		JLabel label = new JLabel("-");
		panel_5.add(label);
		
		txtStopYear = new JTextField();
		panel_5.add(txtStopYear);
		txtStopYear.setHorizontalAlignment(SwingConstants.CENTER);
		txtStopYear.setColumns(4);
		GridBagConstraints gbc_panel_5 = new GridBagConstraints();
		gbc_panel_5.gridwidth = 2;
		gbc_panel_5.anchor = GridBagConstraints.NORTHWEST;
		gbc_panel_5.insets = new Insets(0, 0, 5, 5);
		gbc_panel_5.gridx = 0;
		gbc_panel_5.gridy = 4;
		panel.add(panel_5, gbc_panel_5);
		
		JPanel panel_1 = new JPanel();
		panel_1.setAlignmentX(Component.LEFT_ALIGNMENT);
		panel_1.setPreferredSize(new Dimension(300, 32));
		panel_1.setMinimumSize(new Dimension(300, 0));
		JLabel lblWydawnictwo = new JLabel("Publishing House:");
		panel_1.add(lblWydawnictwo);
		
		txtPubHouse = new JTextField();
		panel_1.add(txtPubHouse);
		txtPubHouse.setHorizontalAlignment(SwingConstants.CENTER);
		txtPubHouse.setColumns(10);
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.gridwidth = 2;
		gbc_panel_1.insets = new Insets(0, 0, 5, 5);
		gbc_panel_1.anchor = GridBagConstraints.NORTHWEST;
		gbc_panel_1.gridx = 0;
		gbc_panel_1.gridy = 5;
		panel.add(panel_1, gbc_panel_1);
		
		JPanel panel_9 = new JPanel();
		GridBagConstraints gbc_panel_9 = new GridBagConstraints();
		gbc_panel_9.anchor = GridBagConstraints.NORTH;
		gbc_panel_9.insets = new Insets(0, 0, 5, 5);
		gbc_panel_9.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel_9.gridx = 0;
		gbc_panel_9.gridy = 6;
		panel.add(panel_9, gbc_panel_9);
		
		JLabel lblGenre = new JLabel("Genre:");
		panel_9.add(lblGenre);
		
		txtGenre = new JTextField();
		panel_9.add(txtGenre);
		txtGenre.setColumns(10);
		
		JPanel panel_7 = new JPanel();
		panel_7.setPreferredSize(new Dimension(300, 32));
		panel_7.setMinimumSize(new Dimension(300, 0));
		panel_7.setAlignmentX(0.0f);
		GridBagConstraints gbc_panel_7 = new GridBagConstraints();
		gbc_panel_7.insets = new Insets(0, 0, 5, 5);
		gbc_panel_7.fill = GridBagConstraints.BOTH;
		gbc_panel_7.gridx = 0;
		gbc_panel_7.gridy = 7;
		panel.add(panel_7, gbc_panel_7);
		
		btnEditAuthors = new JButton("Edit Authors");
		panel_7.add(btnEditAuthors);
		
		btnEditPublishingHouses = new JButton("Edit Publishing Houses");
		panel_7.add(btnEditPublishingHouses);
		
		btnEditGenres = new JButton("Edit Genres");
		panel_7.add(btnEditGenres);
		
		JPanel panel_6 = new JPanel();
		panel_6.setPreferredSize(new Dimension(300, 32));
		panel_6.setMinimumSize(new Dimension(300, 0));
		panel_6.setAlignmentX(0.0f);
		GridBagConstraints gbc_panel_6 = new GridBagConstraints();
		gbc_panel_6.insets = new Insets(0, 0, 0, 5);
		gbc_panel_6.anchor = GridBagConstraints.SOUTH;
		gbc_panel_6.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel_6.gridx = 0;
		gbc_panel_6.gridy = 8;
		panel.add(panel_6, gbc_panel_6);
		
		btnAddBook = new JButton("Add Book");
		panel_6.add(btnAddBook);
		
		menu = new JPopupMenu();
		
		reloadMainTable();
	}

	private final void reloadMainTable() {
		final Object[][] tableData = library.getExplodedBooks();
		@SuppressWarnings("serial")
		DefaultTableModel model = new DefaultTableModel(tableData,library.getBookListHeader()) {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		table = new JTable(model);
		
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setPreferredSize(new Dimension(800, 403));
		splitPane.setRightComponent(scrollPane);
	}

	/**
	 * Set visibility of window
	 */
	public void setVisible(boolean b) { this.frame.setVisible(b); }
}
