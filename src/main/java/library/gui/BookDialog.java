package library.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.GridBagLayout;

import javax.swing.JLabel;

import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JTextField;
import javax.swing.JComboBox;

import library.Library;
import library.data.Author;
import library.data.Book;
import library.data.BookGenre;
import library.data.PubHouse;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

@SuppressWarnings("serial")
public class BookDialog extends JDialog {
	private final JPanel contentPanel = new JPanel();
	
	private JTextField txtTitle;
	private JTextField txtYear;
	private JComboBox<Object> authorComboBox;
	private JComboBox<Object> pubHouseComboBox;
	private JComboBox<Object> genreComboBox;
	private JButton okButton;
	private JButton cancelButton;
	
	private final String newItemString = "<-- new -->";
	private final String noneItemString = "";

	private Library library;
	private Book book = null;

	
	public BookDialog(Library library,  Book book) {
		this.library = library;
		this.book = book;
		initialize();
		initializeListeners();
		
		if(book != null) {
			txtTitle.setText(book.getTitle());
			txtYear.setText(book.getPubYear().toString());
			authorComboBox.setSelectedItem(book.getAuthor());
			pubHouseComboBox.setSelectedItem(book.getPubHouse());
			genreComboBox.setSelectedItem(book.getGenre());
		} 
	}
	/**
	 * @wbp.parser.constructor
	 */
	public BookDialog(Library library) {
		this.library = library;
		initialize();
		initializeListeners();
		

	}
	private void refreshCombos() {
		authorComboBox.removeAllItems();
		authorComboBox.addItem(noneItemString);
		for (Author author: library.getAuthorList()) {
			authorComboBox.addItem(author);
		}
		authorComboBox.addItem(newItemString);
		
		pubHouseComboBox.removeAllItems();
		pubHouseComboBox.addItem(noneItemString);
		for (PubHouse pubHouse: library.getPubHouseList()) {
			pubHouseComboBox.addItem(pubHouse);
		}
		pubHouseComboBox.addItem(newItemString);
		
		genreComboBox.removeAllItems();
		genreComboBox.addItem(noneItemString);
		for (BookGenre genre: library.getGenreList()) {
			genreComboBox.addItem(genre);
		}
		genreComboBox.addItem(newItemString);
		
	}
	private void initializeListeners() {
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Book tmpBook;
				if (book == null) {
					tmpBook = new Book();
				} else {
					tmpBook = book;
				}
				boolean ifCorrect = true;
				if (txtTitle.getText().equals("")) {
					ifCorrect = false;
					txtTitle.setBackground(new Color(0xAA0000));
				} else {
					txtTitle.setBackground(new Color(0xFFFFFF));
				}
				Integer year = null;
				if (!txtYear.getText().equals("")) {
					try {
						year = Integer.parseInt(txtYear.getText());
						txtYear.setBackground(new Color(0xFFFFFF));
					} catch (NumberFormatException ex) {
						ifCorrect = false;
						txtYear.setBackground(new Color(0xAA0000));
					}
				}
				if (ifCorrect) {
					tmpBook.setPubYear(year);
					tmpBook.setTitle(txtTitle.getText());
					if (authorComboBox.getSelectedItem() instanceof Author)
						tmpBook.setAuthor((Author) authorComboBox
								.getSelectedItem());
					if (genreComboBox.getSelectedItem() instanceof BookGenre)
						tmpBook.setGenre((BookGenre) genreComboBox
								.getSelectedItem());
					if (pubHouseComboBox.getSelectedItem() instanceof PubHouse)
						tmpBook.setPubHouse((PubHouse) pubHouseComboBox
								.getSelectedItem());
					library.saveOrUpdate(tmpBook);
					BookDialog.this.dispose();
				}
			}
		});

		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				BookDialog.this.dispose();
			}
		});

		authorComboBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent event) {
				if (event.getStateChange() == ItemEvent.SELECTED) {
					if (event.getItem() == newItemString) {
						JDialog dialog = new AuthorDialog(library);
						dialog.setVisible(true);
						refreshCombos();
					}
				}
			}
		});
		
		pubHouseComboBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent event) {
		       if (event.getStateChange() == ItemEvent.SELECTED) {
		          if(event.getItem() == newItemString) {
						JDialog dialog = new PubHouseDialog(library);
						dialog.setVisible(true);
						refreshCombos();
		          }
		       }
			}
		});
		
		genreComboBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent event) {
		       if (event.getStateChange() == ItemEvent.SELECTED) {
		          if(event.getItem() == newItemString) {
						JDialog dialog = new GenreDialog(library);
						dialog.setVisible(true);
						refreshCombos();
		          }
		       }
			}
		});
	}
	/**
	 * Create the dialog.
	 */
	private void initialize() {
		setModal(true);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.NORTH);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[]{0, 0, 0};
		gbl_contentPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
		gbl_contentPanel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_contentPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPanel.setLayout(gbl_contentPanel);
		{
			JLabel lblTitle = new JLabel("Title:");
			GridBagConstraints gbc_lblTitle = new GridBagConstraints();
			gbc_lblTitle.anchor = GridBagConstraints.EAST;
			gbc_lblTitle.insets = new Insets(0, 0, 5, 5);
			gbc_lblTitle.gridx = 0;
			gbc_lblTitle.gridy = 0;
			contentPanel.add(lblTitle, gbc_lblTitle);
		}
		{
			txtTitle = new JTextField();
			GridBagConstraints gbc_textField = new GridBagConstraints();
			gbc_textField.insets = new Insets(0, 0, 5, 0);
			gbc_textField.fill = GridBagConstraints.HORIZONTAL;
			gbc_textField.gridx = 1;
			gbc_textField.gridy = 0;
			contentPanel.add(txtTitle, gbc_textField);
			txtTitle.setColumns(10);
		}
		{
			JLabel lblAuthor = new JLabel("Author:");
			GridBagConstraints gbc_lblAuthor = new GridBagConstraints();
			gbc_lblAuthor.anchor = GridBagConstraints.EAST;
			gbc_lblAuthor.insets = new Insets(0, 0, 5, 5);
			gbc_lblAuthor.gridx = 0;
			gbc_lblAuthor.gridy = 1;
			contentPanel.add(lblAuthor, gbc_lblAuthor);
		}
		{
			authorComboBox = new JComboBox<Object>();
			GridBagConstraints gbc_comboBox = new GridBagConstraints();
			gbc_comboBox.insets = new Insets(0, 0, 5, 0);
			gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
			gbc_comboBox.gridx = 1;
			gbc_comboBox.gridy = 1;
			contentPanel.add(authorComboBox, gbc_comboBox);
		}
		{
			JLabel lblNewLabel = new JLabel("Year:");
			GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
			gbc_lblNewLabel.anchor = GridBagConstraints.EAST;
			gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
			gbc_lblNewLabel.gridx = 0;
			gbc_lblNewLabel.gridy = 2;
			contentPanel.add(lblNewLabel, gbc_lblNewLabel);
		}
		{
			txtYear = new JTextField();
			GridBagConstraints gbc_txtDsad = new GridBagConstraints();
			gbc_txtDsad.insets = new Insets(0, 0, 5, 0);
			gbc_txtDsad.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtDsad.gridx = 1;
			gbc_txtDsad.gridy = 2;
			contentPanel.add(txtYear, gbc_txtDsad);
			txtYear.setColumns(10);
		}
		{
			JLabel lblPublishingHouse = new JLabel("Publishing House:");
			GridBagConstraints gbc_lblPublishingHouse = new GridBagConstraints();
			gbc_lblPublishingHouse.anchor = GridBagConstraints.EAST;
			gbc_lblPublishingHouse.insets = new Insets(0, 0, 5, 5);
			gbc_lblPublishingHouse.gridx = 0;
			gbc_lblPublishingHouse.gridy = 3;
			contentPanel.add(lblPublishingHouse, gbc_lblPublishingHouse);
		}
		{
			pubHouseComboBox = new JComboBox<Object>();
			GridBagConstraints gbc_comboBox = new GridBagConstraints();
			gbc_comboBox.insets = new Insets(0, 0, 5, 0);
			gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
			gbc_comboBox.gridx = 1;
			gbc_comboBox.gridy = 3;
			contentPanel.add(pubHouseComboBox, gbc_comboBox);
		}
		{
			JLabel lblGenre = new JLabel("Genre:");
			GridBagConstraints gbc_lblGenre = new GridBagConstraints();
			gbc_lblGenre.anchor = GridBagConstraints.EAST;
			gbc_lblGenre.insets = new Insets(0, 0, 0, 5);
			gbc_lblGenre.gridx = 0;
			gbc_lblGenre.gridy = 4;
			contentPanel.add(lblGenre, gbc_lblGenre);
		}
		{
			genreComboBox = new JComboBox<Object>();
			GridBagConstraints gbc_comboBox = new GridBagConstraints();
			gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
			gbc_comboBox.gridx = 1;
			gbc_comboBox.gridy = 4;
			contentPanel.add(genreComboBox, gbc_comboBox);
		}
		refreshCombos();
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

}
