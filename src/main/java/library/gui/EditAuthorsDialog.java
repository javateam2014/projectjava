package library.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import library.Library;
import library.data.Author;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.ListSelectionModel;

@SuppressWarnings({"serial"})
public class EditAuthorsDialog extends JDialog {
	private JButton button;

	private Library library;
	private JList<Author> list;

	private DefaultListModel<Author> listModel;

	private JButton btnNew;

	
	/**
	 * @wbp.parser.constructor
	 */
	public EditAuthorsDialog(Library library) {
		this.library = library;
		initialize();
		initializeListeners();
		

	}

	private void initializeListeners() {
		list.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent event) {
				if (list.getModel().getSize() > 0) {
					int index = list.locationToIndex(event.getPoint());
					if (event.getButton() == 3) {

						if (list.getSelectedIndex() != index)
							list.setSelectedIndex(index);
						if (list.getCellBounds(0, list.getLastVisibleIndex())
								.contains(event.getPoint())) {
							
							JPopupMenu menu = new JPopupMenu();
							
							JMenuItem edit = new JMenuItem("edit");
							edit.addActionListener(new ActionListener() {
								@Override
								public void actionPerformed(ActionEvent e) {
									JDialog dialog = new AuthorDialog(library,
											list.getSelectedValue());
									dialog.setVisible(true);
									reload();
								}

							});
							menu.add(edit);
							
							JMenuItem delete = new JMenuItem("delete");
							delete.addActionListener(new ActionListener() {
								@Override
								public void actionPerformed(ActionEvent e) {
									library.delete(list.getSelectedValue());
									reload();

								}
							});
							menu.add(delete);

							menu.show(list, event.getX(), event.getY());
						}
					}
				}
			}
		});
		
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				EditAuthorsDialog.this.dispose();
			}
		});
		btnNew.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JDialog dialog = new AuthorDialog(library);
				dialog.setVisible(true);
				reload();
			}
		});

	}
	/**
	 * Create the dialog.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void initialize() {
		setModal(true);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				btnNew = new JButton("New");
				buttonPane.add(btnNew);
			}
			{
				button = new JButton("Exit");
				button.setHorizontalAlignment(SwingConstants.RIGHT);
				button.setActionCommand("Exit");
				buttonPane.add(button);
				getRootPane().setDefaultButton(button);
			}
		}
		listModel = new DefaultListModel<>();
		list = new JList(listModel);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		reload();

		JScrollPane scrollPane = new JScrollPane(list);
		getContentPane().add(scrollPane, BorderLayout.CENTER);
	}
	
	private void reload() {
		listModel.removeAllElements();;
		for (Author obj: library.getAuthorList())
			listModel.addElement(obj);
	}

}
