package library.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import library.Library;
import library.data.PubHouse;

@SuppressWarnings({"serial"})
public class PubHouseDialog extends JDialog {
	private final JPanel contentPanel = new JPanel();
	
	private JTextField txtName;
	private JButton okButton;
	private JButton cancelButton;

	private Library library;
	private PubHouse pubHouse = null;

	
	public PubHouseDialog(Library library,  PubHouse pubHouse) {
		this.library = library;
		initialize();
		initializeListeners();
		
		if(pubHouse != null) {
			txtName.setText(pubHouse.getName());
		} 
	}
	/**
	 * @wbp.parser.constructor
	 */
	public PubHouseDialog(Library library) {
		this.library = library;
		initialize();
		initializeListeners();
		

	}
	private void initializeListeners() {
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(pubHouse == null) {
					pubHouse = new PubHouse();
				}
				boolean ifCorrect = true;
				if(txtName.getText().equals("")) {
					ifCorrect = false;
					txtName.setBackground(new Color(0xAA0000));
				} else {
					pubHouse.setName(txtName.getText());
					txtName.setBackground(new Color(0xFFFFFF));
				}
				if(ifCorrect) {
					pubHouse.setName(txtName.getText());
					library.saveOrUpdate(pubHouse);
					PubHouseDialog.this.dispose();
				}
			}
		});

		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				PubHouseDialog.this.dispose();
			}
		});
	}
	/**
	 * Create the dialog.
	 */
	private void initialize() {
		setModal(true);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.NORTH);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[]{0, 0, 0};
		gbl_contentPanel.rowHeights = new int[]{0, 0, 0};
		gbl_contentPanel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_contentPanel.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		contentPanel.setLayout(gbl_contentPanel);
		{
			JLabel lblTitle = new JLabel("Name:");
			GridBagConstraints gbc_lblTitle = new GridBagConstraints();
			gbc_lblTitle.anchor = GridBagConstraints.EAST;
			gbc_lblTitle.insets = new Insets(0, 0, 5, 5);
			gbc_lblTitle.gridx = 0;
			gbc_lblTitle.gridy = 0;
			contentPanel.add(lblTitle, gbc_lblTitle);
		}
		{
			txtName = new JTextField();
			txtName.setText("Publishing house Name");
			GridBagConstraints gbc_textField = new GridBagConstraints();
			gbc_textField.anchor = GridBagConstraints.NORTH;
			gbc_textField.insets = new Insets(0, 0, 5, 0);
			gbc_textField.fill = GridBagConstraints.HORIZONTAL;
			gbc_textField.gridx = 1;
			gbc_textField.gridy = 0;
			contentPanel.add(txtName, gbc_textField);
			txtName.setColumns(10);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

}
