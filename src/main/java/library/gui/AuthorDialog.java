package library.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.GridBagLayout;

import javax.swing.JLabel;

import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JTextField;

import library.Library;
import library.data.Author;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

@SuppressWarnings({"serial", "deprecation"})
public class AuthorDialog extends JDialog {
	private final JPanel contentPanel = new JPanel();
	
	private JTextField txtName;
	private JTextField txtDay;
	private JTextField txtMonth;
	private JTextField txtYear;
	private JButton okButton;
	private JButton cancelButton;

	private Library library;
	private Author author = null;
	private JPanel panel;
	private JLabel label;
	private JLabel label_1;

	
	public AuthorDialog(Library library,  Author author) {
		this.library = library;
		initialize();
		initializeListeners();
		
		if(author != null) {
			txtName.setText(author.getName());
			Date date = author.getBirthDate();
			if(date != null) {
				txtDay.setText(Integer.toString(date.getDay()));
				txtMonth.setText(Integer.toString(date.getMonth()));
				txtYear.setText(Integer.toString(date.getYear()));
			}
		} 
	}
	/**
	 * @wbp.parser.constructor
	 */
	public AuthorDialog(Library library) {
		this.library = library;
		initialize();
		initializeListeners();
		

	}
	private void initializeListeners() {
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(author == null) {
					author = new Author();
				}
				boolean ifCorrect = true;
				if(txtName.getText().equals("")) {
					ifCorrect = false;
					txtName.setBackground(new Color(0xAA0000));
				} else {
					author.setName(txtName.getText());
					txtName.setBackground(new Color(0xFFFFFF));
				}
				Integer day = null,month = null,year = null;
				if(!txtDay.getText().equals("")
						|| !txtDay.getText().equals("")
						|| !txtDay.getText().equals("")) {
					try {
						day = Integer.parseInt(txtDay.getText());
						txtDay.setBackground(new Color(0xFFFFFF));
					} catch (NumberFormatException ex) {
						ifCorrect = false;
						txtDay.setBackground(new Color(0xAA0000));
					}

					try {
						month = Integer.parseInt(txtMonth.getText());
						txtMonth.setBackground(new Color(0xFFFFFF));
					} catch (NumberFormatException ex) {
						ifCorrect = false;
						txtMonth.setBackground(new Color(0xAA0000));
					}

					try {
						year = Integer.parseInt(txtYear.getText());
						txtYear.setBackground(new Color(0xFFFFFF));
					} catch (NumberFormatException ex) {
						ifCorrect = false;
						txtYear.setBackground(new Color(0xAA0000));
					}
				}
				if(ifCorrect) {
					author.setName(txtName.getText());
					if(day != null && month != null && year != null)
						author.setBirthDate(new Date(year,month,day));
					library.saveOrUpdate(author);
					AuthorDialog.this.dispose();
				}
			}
		});

		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				AuthorDialog.this.dispose();
			}
		});
	}
	/**
	 * Create the dialog.
	 */
	private void initialize() {
		setModal(true);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.NORTH);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[]{0, 0, 0};
		gbl_contentPanel.rowHeights = new int[]{0, 0, 0};
		gbl_contentPanel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_contentPanel.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		contentPanel.setLayout(gbl_contentPanel);
		{
			JLabel lblTitle = new JLabel("Name:");
			GridBagConstraints gbc_lblTitle = new GridBagConstraints();
			gbc_lblTitle.anchor = GridBagConstraints.EAST;
			gbc_lblTitle.insets = new Insets(0, 0, 5, 5);
			gbc_lblTitle.gridx = 0;
			gbc_lblTitle.gridy = 0;
			contentPanel.add(lblTitle, gbc_lblTitle);
		}
		{
			txtName = new JTextField();
			txtName.setText("Author Name");
			GridBagConstraints gbc_textField = new GridBagConstraints();
			gbc_textField.anchor = GridBagConstraints.NORTH;
			gbc_textField.insets = new Insets(0, 0, 5, 0);
			gbc_textField.fill = GridBagConstraints.HORIZONTAL;
			gbc_textField.gridx = 1;
			gbc_textField.gridy = 0;
			contentPanel.add(txtName, gbc_textField);
			txtName.setColumns(10);
		}
		{
			JLabel lblNewLabel = new JLabel("Birth date:");
			GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
			gbc_lblNewLabel.anchor = GridBagConstraints.EAST;
			gbc_lblNewLabel.insets = new Insets(0, 0, 0, 5);
			gbc_lblNewLabel.gridx = 0;
			gbc_lblNewLabel.gridy = 1;
			contentPanel.add(lblNewLabel, gbc_lblNewLabel);
		}
		{
			panel = new JPanel();
			GridBagConstraints gbc_panel = new GridBagConstraints();
			gbc_panel.anchor = GridBagConstraints.WEST;
			gbc_panel.fill = GridBagConstraints.VERTICAL;
			gbc_panel.gridx = 1;
			gbc_panel.gridy = 1;
			contentPanel.add(panel, gbc_panel);
			panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
			{
				txtDay = new JTextField();
				txtDay.setText("DD");
				panel.add(txtDay);
				txtDay.setColumns(2);
			}
			{
				label = new JLabel(" - ");
				panel.add(label);
			}
			{
				txtMonth = new JTextField();
				txtMonth.setText("MM");
				txtMonth.setColumns(2);
				panel.add(txtMonth);
			}
			{
				label_1 = new JLabel(" - ");
				panel.add(label_1);
			}
			{
				txtYear = new JTextField();
				txtYear.setText("YYYY");
				txtYear.setColumns(4);
				panel.add(txtYear);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

}
